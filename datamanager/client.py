import requests as r
from datetime import datetime
from tqdm import tqdm
import concurrent.futures as cf
from pathlib import Path
import pandas as pd
import numpy as np
from operator import itemgetter
from time import sleep

from .helpers import MyParser


class DataManager():
    #api wrapper
    def __init__(self, net_key, in_key):
        self.net_url = f"https://api.phish.net/v3"
        self.parser = MyParser()
        self.data = {
            'net_key' : net_key,
            'net_url' : f"https://api.phish.net/v5",
            'in_key' : in_key,
            'in_url' : 'http://phish.in/api/v1/'
        }
        self.eras = self._get_eras()
        self.song_info = self._get_songs()

    def pull_data(self, start_date, end_date='present', result_dir = 'results'):
        """pull down data starting at some date
        args:
            start_date (str/datetime): earliest date to grab data from
            result_dir (str/pathlib.Path): place to save results
        returns:
            success (bool): success/failure of algorithm
        """
        self.show_dates = self._get_showdates(start_date)
        self.setlists = dict()
        with tqdm(total=len(self.show_dates)) as pbar:
            with cf.ThreadPoolExecutor() as e:
                #results = {x: e.submit(self.get_setlist, x) for x in self.show_dates}
                results = {e.submit(self.get_setlist, x): x for x in self.show_dates}
                for future in cf.as_completed(results):
                    pbar.update(1)
                    show = results[future]
                    data = future.result()
                    self.setlists[show] = data

        # TODO this is bandaid from get_setlist returning None sometimes when it shouldn't have failed
        # for some reason some are failing, so 
        failed = [key for key, value in self.setlists.items() if value is None]
        for date in failed:
            self.setlists[date] = self.get_setlist(date)

        df = pd.concat([value for key, value in self.setlists.items()])
        result_dir = Path(result_dir)
        result_dir.mkdir(parents=True, exist_ok=True)
        df.to_csv(result_dir / 'phish.csv', index=False)


    # API ENDPOINTS
    def hit_endpoint(self, endpoint, additional):
        # Ex:
        #   /shows/artist/phish
        #   /songs
        #   /setlists/showdate/2021-10-31
        address = f"{self.data['net_url']}/{endpoint}"
        if additional is not None:
            address += f"/{additional}"
        response = r.get(address,
            params={'apikey': self.data['net_key']}).json()
        return response

    def _get_shows(self, per_page, page):
        """access show data stored at phish.in, api is setup to return shows by db page and # of shows per page
        args:
            per_page (int): number of shows per page
            page (int): which page
        returns:
            message (requests.models.Response): api response
        """
        message = r.get(self.data['in_url'] + '/shows',
            headers={
                'Authorization': f'Bearer {self.data["in_key"]}',
                'Accept': 'application/json'
                },
            params={
                'sort_attr': 'date',
                'sort_dir': 'desc',
                'per_page': per_page,
                'page': page
        })
        return message

    def _get_eras(self):
        """return dictionary that maps era -> years
        args:
            NONE
        returns:
            eras (dict): dictionary of {era: [years]}
        """
        response = r.get(f"{self.data['in_url']}/eras", headers={
            "Accept": "application/json",
            "Authorization": f"Bearer {self.data['in_key']}"
        })
        eras = response.json()['data']
        return eras

    def get_setlist(self, date):
        """Returns showdata if there is a show, returns None is there isn't
        args:
            date: str - showdate
        """
        response = self.hit_endpoint('setlists', f"showdate/{date}")
        if response['error']:
            raise ValueError(f"Get setlist failure: {response['error_message']}")
        try:
            setlist = response['data']
            # 'showid', 'showdate', 'permalink', 'showyear', 
            # 'uniqueid', 'meta', 'reviews', 'exclude', 'setlistnotes', 
            # 'soundcheck', 'songid', 'position', 'transition', 'footnote', 
            # 'set', 'isjam', 'isreprise', 'isjamchart', 'jamchart_description', 
            # 'tracktime', 'gap', 'tourid', 'tourname', 'tourwhen', 'song', 
            # 'nickname', 'slug', 'is_original', 'venueid', 'venue', 'city', 
            # 'state', 'country', 'trans_mark', 'artistid', 'artist_slug', 'artist_name'
            keys = ['song',  'nickname', 'position', 'set', \
                    'gap', 'is_original', 'isjam', 'isjamchart', 'isreprise',\
                    'slug', 'songid', 'tourid', 'tracktime', 'trans_mark', 'transition',\
                    'venue', 'venueid', 'showid'] + \
                    ['artist_name', 'showdate', 'showyear', 'city', 'state', 'country', 'tourid', 'venue', 'venueid'] # repeated information

            show = [{y: x[y] for y in keys} for x in setlist]
            show = pd.DataFrame([x for x in reversed(show)]) # shuffling order is more intuitive
            show['weekday'] = self._datetime(date).weekday()
            
            # add songinfo for each row
            song_info = ['debut', 'times_played']
            show[song_info] = [[itemgetter(y)(self.song_info[x]) for y in song_info] for x in show.song]

            return show
        except:
            return None


    def _get_songs(self):
        """Grabs information from the .net/songs endpoint.
        Returns
            songs (dict): {song: song_info} where song_info is a dictionary with the keys: songid, 
        """
        response = r.get(f"{self.data['net_url']}/songs?apikey={self.data['net_key']}").json()['data']
        return {x['song']: x for x in response}

    # private
    def _datetime(self, date):
        """return datetime object from a date like -> '2018-11-03'
        """
        return datetime(*[int(x) for x in date.split('-')])

    def _get_showdates(self, start_date):
        """populate show_dates with all shows going back to supplied start_date arg."""
        # loop through the others until we reach the end date
        show_dates = []
        # we have to do an api call to get the total # of pages in the db, so we will start gathering our data with that first call
        message = self._get_shows(100, page=1)
        show_dates = [x['date'] for x in message.json()['data'] if 
            self._datetime(x['date']) > self._datetime(start_date)]
        total_pages = message.json()['total_pages']
        current_page = 2
        with tqdm(total=total_pages, desc='Getting showdates.') as pbar:
            pbar.update(1)
            while current_page <= total_pages:
                message = self._get_shows(100, page=current_page)
                show_dates += [x['date'] for x in message.json()['data'] if 
                    self._datetime(x['date']) > self._datetime(start_date)]
                current_page += 1
                if self._datetime(message.json()['data'][-1]['date']) < self._datetime(start_date):
                    print("\nHit start_date, terminating early!")
                    pbar.close()
                    break
                pbar.update(1)
        # while current_page <= total_pages:
        #     message = self._get_shows(100, page=current_page)
        #     show_dates += [x['date'] for x in message.json()['data'] if
        #         self._datetime(x['date']) > self._datetime(start_date)]
        #     current_page +=1
        #     if self._datetime(message.json()['data'][-1]['date']) < self._datetime(start_date):
        #         break
                
        return show_dates
