FROM python:3.9

WORKDIR /src

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get install -y --no-install-recommends make && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    pip install --no-cache-dir --upgrade pip && \
    pip install --no-cache-dir pipenv

# copy all files
COPY . /src

# install dependencies
RUN pip install -r requirements.txt

# install package
RUN pip install .