$MyInvocation.MyCommand.Path | Split-Path | Push-Location
docker build -t datamanager:builder .
docker run --name datamanager-builder datamanager:builder python setup.py bdist_wheel
docker cp datamanager-builder:src/dist/ .
docker container rm -f datamanager-builder