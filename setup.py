from setuptools import setup

with open('requirements.txt') as f:
    required = f.read().splitlines()

setup(
    name='datamanager',
    version='0.1.0',
    description='A client package to obtain phish data',
    author='Robert Reaney',
    packages=['datamanager'],
    install_requires=required,
    classifiers=['Programming Language :: Python :: 3.9']
)