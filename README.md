# Liscense

This data is available as a courtesy by `The Mockingbird Foundation` via `Phish.net`.

# Data Scraper

A python module that obtains setlist data for performances by Phish. Uses `phish.net` and `phish.in` api to serve data to user.

# Delivery

This repo is available as a `.whl` file delivered as a ci/cd artifact and locally via `build_whl.ps`

# To Do

Make this into a database with a watcher the pings api to detect changes to available showdates and updates db accordingly.