from datamanager import DataManager
from absl import flags, app

FLAGS = flags.FLAGS
flags.DEFINE_string("start_date", '1980-01-01', 'Earliest date to obtain data frome. Default: 1980-01-01')

# read environment variables
with open('.env', 'r') as f:
    ENV = {x.split('=')[0]: x.split('=')[1].strip() for x in f.readlines()}

def main(argv):
    client = DataManager(net_key = ENV['NET_KEY'], in_key = ENV['IN_KEY'])
    client.pull_data(FLAGS.start_date)

if __name__ == '__main__':
    app.run(main)