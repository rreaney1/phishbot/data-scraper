from html.parser import HTMLParser

class MyParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.refresh()
    def refresh(self):
        self.data = {
            "set": [],
            "song": []
        }
    def handle_starttag(self, tag, attrs):
        self._current_tag = tag
    def handle_data(self, data):
        if self._current_tag == 'span':
            if data != ": ":
                self._current_set = data
        elif self._current_tag == 'a':
            if all(x.isalpha() or x.isspace() or x.isnumeric() or x == "'" for x in data):
                self.data['song'].append(data)
                self.data['set'].append(self._current_set)
            # else:
            #     print(f"current segue: {data}")
            #     self.data['segue'].append(data.strip())