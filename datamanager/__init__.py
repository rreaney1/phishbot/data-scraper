"""datamanager

A client package for the Phish.net/.in APIs
"""

__version__ = "0.1.0"
__author__ = "Robert Reaney"

from .client import DataManager